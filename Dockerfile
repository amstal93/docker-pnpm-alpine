FROM node:alpine
RUN apk add --no-cache curl && \
  curl -L https://unpkg.com/@pnpm/self-installer | node && \
  apk del curl
